// ignore_for_file: curly_braces_in_flow_control_structures

import 'package:flutter/material.dart';

void main() {
  runApp(App());
}

class App extends StatefulWidget {
  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  final txtPeso = TextEditingController();
  final txtAltura = TextEditingController();

  var imagem = 'images/obesidade2.jpg';
  double? imc;
  String? mensagem;

  void calculaImc() {
    var peso = double.parse(txtPeso.text);
    var altura = double.parse(txtAltura.text);

    setState(() => imc = peso / (altura * altura));

    if (imc! < 18.5)
      setState(() {
        imagem = 'images/abaixo.jpg';
        mensagem = 'Abaixo do peso.';
      });
    else if (imc! < 24.9)
      setState(() {
        imagem = 'images/ideal.jpg';
        mensagem = 'Peso Ideal.';
      });
    else if (imc! < 29.9)
      setState(() {
        imagem = 'images/excesso.jpg';
        mensagem = 'Excesso peso.';
      });
    else if (imc! < 39.9)
      setState(() {
        imagem = 'images/obesidade1.jpg';
        mensagem = 'Obesidade I.';
      });
    else
      setState(() {
        imagem = 'images/obesidade2.jpg';
        mensagem = 'Obesidade II';
      });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text("Cálculo IMC"),
        ),
        body: Column(
          children: [
            Flexible(
              flex: 1,
              child: Container(
                margin: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Peso'),
                    TextField(
                      keyboardType: TextInputType.number,
                      controller: txtPeso,
                    ),
                    Text('Altura'),
                    TextField(
                      controller: txtAltura,
                    ),
                    Container(
                      width: double.infinity,
                      child: ElevatedButton(
                        child: Text("Calcular"),
                        onPressed: calculaImc,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: imc == null
                  ? Container()
                  : Stack(
                    fit: StackFit.expand,
                    children: [
                      FittedBox(child: Image.asset(imagem)),
                      Center(
                        child: GestureDetector(
                          onTap: () {
                            setState(() => imc = null);
                          },
                          child: Container(
                            child: Text(
                              "$mensagem\nIMC: ${imc!.toStringAsFixed(2)}",
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 22)
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
            ),
          ],
        ),
      ),
    );
  }
}
